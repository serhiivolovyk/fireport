package com.example.fireport.handler;

import com.example.fireport.entity.FireReport;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
@AllArgsConstructor
public class ElasticFacade {

    private final RestTemplate restTemplate;

    void create(FireReport fireReport) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            restTemplate.exchange("http://192.168.31.18:9200/fireport/fire", HttpMethod.POST, new HttpEntity<>(fireReport, headers), String.class);
        } catch (Exception ex) {
            if (ex instanceof HttpClientErrorException) {
                System.out.println(((HttpClientErrorException) ex).getResponseBodyAsString());
            }
        }
    }

}
