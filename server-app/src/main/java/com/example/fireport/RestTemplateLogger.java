package com.example.fireport;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class RestTemplateLogger implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {
        log(request, body);
        ClientHttpResponse clientHttpResponse = execution.execute(request, body);
        log(clientHttpResponse);
        return clientHttpResponse;
    }

    private void log(HttpRequest request, byte[] body) {
        System.out.println("Request: " + toString(request, body));
    }

    private void log(ClientHttpResponse response) {
        System.out.println("Response: " + toString(response));
    }

    private String getRequestBody(byte[] body) {
        return body != null && body.length > 0 ? new String(body, StandardCharsets.UTF_8) : null;
    }

    private String getResponseBody(ClientHttpResponse response) {
        try {
            return StreamUtils.copyToString(response.getBody(), Charset.forName("UTF-8"));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

    private String toString(HttpRequest request, byte[] body) {
        return request.getMethod() + " " + request.getURI() + " Headers=" + request.getHeaders() + " Body=" + getRequestBody(body);
    }


    private String toString(ClientHttpResponse response) {
        return getStatus(response) + " Headers=" + response.getHeaders() + " Body=" + getResponseBody(response);
    }

    private String getStatus(ClientHttpResponse response) {
        try {
            return response.getStatusCode() + " " + response.getStatusText();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }

}
