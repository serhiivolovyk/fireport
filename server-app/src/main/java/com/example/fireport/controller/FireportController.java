package com.example.fireport.controller;

import com.example.fireport.entity.FireReportDto;
import com.example.fireport.entity.SubscribeDto;
import com.example.fireport.handler.FireportHandler;
import com.helger.commons.base64.Base64;
import lombok.AllArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@RestController
@AllArgsConstructor
public class FireportController {

    private final FireportHandler fireportHandler;

    @PostMapping("/api/report")
    public String report(@RequestBody FireReportDto body) throws IOException {
        System.out.println("report:" + body);
        String filename = null;
        if (!StringUtils.isEmpty(body.getPhoto())) {
            byte[] image = Base64.decode(body.getPhoto());
            filename = fireportHandler.saveImage(new ByteArrayInputStream(image), "image/png");
        }
        fireportHandler.process(filename, String.valueOf(body.getLatitude()), String.valueOf(body.getLongitude()));
        return "{\"success\":\"true\"}";
    }

    @PostMapping("/api/subscribe")
    public String subscribe(@RequestBody SubscribeDto body) {
        System.out.println("subscribe:" + body);
        fireportHandler.subscribe(body.getEmail(), String.valueOf(body.getLatitude()), String.valueOf(body.getLongitude()));
        return "{\"success\":\"true\"}";
    }

}
