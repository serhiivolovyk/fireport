package com.example.fireport.ui;

import com.example.fireport.handler.FireportHandler;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import lombok.Getter;

@Getter
public class OptionalUpload extends Upload {

    private String filename;

    public OptionalUpload(FireportHandler fireportHandler) {
        MemoryBuffer buffer = new MemoryBuffer();
        setReceiver(buffer);
        setAcceptedFileTypes("image/jpeg", "image/png");

        addSucceededListener(event -> {
            filename = fireportHandler.saveImage(buffer.getInputStream(), event.getMIMEType());
        });
    }

}
