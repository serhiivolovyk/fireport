package com.example.fireport.ui;

import com.example.fireport.handler.FireportHandler;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Input;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class SubscribePanel extends VerticalLayout {

    private HorizontalLayout latLon;
    private Button subscribeB;
    private Button sendB;
    private Label label = new Label("Done!");
    private Input email;
    private boolean visible;

    public SubscribePanel(FireportHandler fireportHandler) {
        setWidth("100%");

        subscribeB = new Button("Subscribe", e -> {
            visible = !visible;
            latLon.setVisible(visible);
            email.setVisible(visible);
            sendB.setVisible(visible);
            remove(label);
        });
        setStyleGrey(subscribeB);

        email = new Input();
        email.setVisible(visible);
        email.setPlaceholder("Email");
        setStyleGrey(email);

        latLon = new HorizontalLayout();
        latLon.setVisible(visible);

        Input lat = new Input();
        setStyleGrey(lat);
        lat.setPlaceholder("Latitude");
        latLon.add(lat);

        Input lon = new Input();
        setStyleGrey(lon);
        lon.setPlaceholder("Longitude");
        latLon.add(lon);

        sendB = new Button("Send", e -> {
            fireportHandler.subscribe(email.getValue(), lat.getValue(), lon.getValue());
            add(label);
        });
        sendB.setVisible(visible);
        setStyleGrey(sendB);

        add(subscribeB);
        add(email);
        add(latLon);
        add(sendB);
    }

    private void setStyleGrey(HasStyle component) {
        component.getStyle().set("background-color", "#666666");
        component.getStyle().set("color", "#999999");
        component.getStyle().set("outline", "none");
        component.getStyle().set("box-shadow", "none");
        component.getStyle().set("border", "none");
    }

}
