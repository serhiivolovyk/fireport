package com.example.fireport.entity;

import lombok.Data;

@Data
public class SubscribeDto {

    private String email;
    private Float latitude;
    private Float longitude;

}
