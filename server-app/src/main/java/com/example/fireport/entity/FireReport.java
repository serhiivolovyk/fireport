package com.example.fireport.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FireReport {

    @JsonProperty("the_type")
    private String theType = "web_user";
    private Long timestamp = System.currentTimeMillis();

    private String latitude;
    private String longitude;
    @JsonProperty("geoip.location")
    private String geoipLocation;
    private String filename;

}
