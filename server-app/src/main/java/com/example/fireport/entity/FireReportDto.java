package com.example.fireport.entity;

import lombok.Data;

@Data
public class FireReportDto {

    private Float latitude;
    private Float longitude;
    private String description;
    private String photo;

}
