package com.example.serhii_volovyk.mlkitsampling.presenter;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.serhii_volovyk.mlkitsampling.R;
import com.example.serhii_volovyk.mlkitsampling.presenter.main.MainActivity;

import java.util.Objects;

public class SplashActivity extends AppCompatActivity {

    final int DURATION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Objects.requireNonNull(getSupportActionBar()).hide();


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);

                finish();
            }
        }, DURATION);
    }
}
