package com.example.serhii_volovyk.mlkitsampling.presenter.report;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.serhii_volovyk.mlkitsampling.R;
import com.example.serhii_volovyk.mlkitsampling.data.dto.ReportDTO;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class ReportActivity extends AppCompatActivity implements ReportContract.View {

    private String TAG = ReportActivity.class.getSimpleName();

    private ReportContract.Presenter presenter;

    private ImageView imageView;

    private EditText latitudeEditText;

    private EditText longitudeEditText;

    private EditText descriptionEditText;

    private Button sendReportButton;

    private ProgressBar progressBar;

    private Bitmap bitmapForProcessing;

    static final private int REQUEST_IMAGE_CAPTURE = 864;

    private String imageFilePath;

    private FusedLocationProviderClient mFusedLocationClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        presenter = new ReportPresenter(this);

        imageView = findViewById(R.id.imageView);

        sendReportButton = findViewById(R.id.sendReportButton);

        latitudeEditText = findViewById(R.id.latEditText);

        longitudeEditText = findViewById(R.id.longEditText);

        descriptionEditText = findViewById(R.id.descriptionEditText);

        progressBar = findViewById(R.id.reportProgressBar);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Location permission problem!", Toast.LENGTH_LONG).show();
            return;
        } else {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                latitudeEditText.setText(String.valueOf(location.getLatitude()));
                                longitudeEditText.setText(String.valueOf(location.getLongitude()));
                            }
                        }
                    });
        }

        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openCameraIntent();
            }
        });

        sendReportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                float latitude = Float.valueOf(latitudeEditText.getText().toString());
                float longitude = Float.valueOf(longitudeEditText.getText().toString());
                String description = descriptionEditText.getText().toString();
                String bitmap = getStringFromBitmap(bitmapForProcessing);
                ReportDTO report = new ReportDTO(latitude, longitude, description, bitmap);
                presenter.send(report);
            }
        });
    }


    private void openCameraIntent() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(pictureIntent.resolveActivity(getPackageManager()) != null){
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Log.e(TAG, "Photo file can't be created");
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.serhii_volovyk.mlkitsampling.provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(pictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,".jpg", storageDir);
        imageFilePath = image.getAbsolutePath();
        return image;
    }

    private void setPic() {
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageFilePath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap smallBitmap = BitmapFactory.decodeFile(imageFilePath, bmOptions);
        imageView.setImageBitmap(smallBitmap);

        Bitmap fullSizeBitmap = BitmapFactory.decodeFile(imageFilePath, bmOptions);
        bitmapForProcessing = fullSizeBitmap;
    }

    private String getStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
        byte[] byte_arr = stream.toByteArray();
        return Base64.encodeToString(byte_arr, 0);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onSendingSuccessful() {
        Toast.makeText(this, "Report sent!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSendingFailure() {
        Toast.makeText(this, "Report failed!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void startSpinner() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopSpinner() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setPresenter(ReportContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            setPic();
        }
    }
}
