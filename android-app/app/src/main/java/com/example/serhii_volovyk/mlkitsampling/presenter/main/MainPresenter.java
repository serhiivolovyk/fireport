package com.example.serhii_volovyk.mlkitsampling.presenter.main;

import android.support.annotation.NonNull;
import android.util.Log;

public class MainPresenter implements MainContract.Presenter {

    private String TAG = MainPresenter.class.getSimpleName();

    private MainContract.View mainView;

    public MainPresenter(@NonNull MainContract.View mainView) {
        this.mainView = mainView;
        this.mainView.setPresenter(this);
    }

    @Override
    public void start() {
        Log.i(TAG, "start called");
    }
}
