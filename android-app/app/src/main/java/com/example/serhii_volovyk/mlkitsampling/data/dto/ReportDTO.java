package com.example.serhii_volovyk.mlkitsampling.data.dto;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

public class ReportDTO {

    @SerializedName("latitude")
    private float latitude;

    @SerializedName("longitude")
    private float longitude;

    @SerializedName("description")
    private String description;

    @SerializedName("photo")
    private String photo;

    public ReportDTO(float latitude, float longitude, String description, String photo) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.photo = photo;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public String getDescription() {
        return description;
    }

    public String getPhoto() {
        return photo;
    }
}
