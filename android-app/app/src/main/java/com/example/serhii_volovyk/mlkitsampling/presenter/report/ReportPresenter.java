package com.example.serhii_volovyk.mlkitsampling.presenter.report;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.serhii_volovyk.mlkitsampling.data.DataSource;
import com.example.serhii_volovyk.mlkitsampling.data.Repository;
import com.example.serhii_volovyk.mlkitsampling.data.dto.ReportDTO;

public class ReportPresenter implements ReportContract.Presenter {

    private String TAG = ReportPresenter.class.getSimpleName();

    private ReportContract.View reportView;

    private Repository repository;

    public ReportPresenter(@NonNull ReportContract.View mainView) {
        this.reportView = mainView;
        this.reportView.setPresenter(this);
        repository = new Repository();
    }

    @Override
    public void send(final ReportDTO report) {
        Log.i(TAG, "Sending report...");
        reportView.startSpinner();
        repository.sendReport(report, new DataSource.SendReportCallback() {
            @Override
            public void onReportSend() {
                reportView.onSendingSuccessful();
                reportView.stopSpinner();
            }

            @Override
            public void onReportFailure() {
                reportView.onSendingFailure();
                reportView.stopSpinner();
            }
        });
        reportView.stopSpinner();
    }

    @Override
    public void start() {
        Log.i(TAG, "start called");
    }
}
