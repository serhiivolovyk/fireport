package com.example.serhii_volovyk.mlkitsampling.presenter.main;

import com.example.serhii_volovyk.mlkitsampling.presenter.BasePresenter;
import com.example.serhii_volovyk.mlkitsampling.presenter.BaseView;

public interface MainContract {

    interface View extends BaseView<Presenter> {

        void startSpinner();

        void stopSpinner();
    }

    interface Presenter extends BasePresenter {
        //No activities yet
    }
}
