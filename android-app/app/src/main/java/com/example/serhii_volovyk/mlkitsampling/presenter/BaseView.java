package com.example.serhii_volovyk.mlkitsampling.presenter;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
