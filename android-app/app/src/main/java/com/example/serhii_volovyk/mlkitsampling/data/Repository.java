package com.example.serhii_volovyk.mlkitsampling.data;

import android.support.annotation.NonNull;

import com.example.serhii_volovyk.mlkitsampling.data.dto.ReportDTO;
import com.example.serhii_volovyk.mlkitsampling.data.dto.ResponseDTO;
import com.example.serhii_volovyk.mlkitsampling.data.dto.SubscriptionDTO;
import com.example.serhii_volovyk.mlkitsampling.data.rest.ApiClient;
import com.example.serhii_volovyk.mlkitsampling.data.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Repository implements DataSource {

    private final String TAG = Repository.class.getSimpleName();

    @Override
    public void sendReport(@NonNull ReportDTO report, @NonNull final SendReportCallback callback) {
        ApiClient.getApiInterface().sendReport(report).enqueue(new Callback<ResponseDTO>() {
            @Override
            public void onResponse(Call<ResponseDTO> call, Response<ResponseDTO> response) {
                callback.onReportSend();
            }

            @Override
            public void onFailure(Call<ResponseDTO> call, Throwable t) {
                callback.onReportFailure();
            }
        });
    }

    @Override
    public void subscribeToFireport(@NonNull SubscriptionDTO subscription, @NonNull final SubscriptionCallback callback) {
        ApiClient.getApiInterface().sendSubscription(subscription).enqueue(new Callback<ResponseDTO>() {
            @Override
            public void onResponse(Call<ResponseDTO> call, Response<ResponseDTO> response) {
                callback.onSubscriptionSuccess();
            }

            @Override
            public void onFailure(Call<ResponseDTO> call, Throwable t) {
                callback.onSubscriptionFailure();
            }
        });
    }
}
