package com.example.serhii_volovyk.mlkitsampling.presenter;

public interface BasePresenter {

    void start();

}
