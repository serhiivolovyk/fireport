package com.example.serhii_volovyk.mlkitsampling.data.rest;

import com.example.serhii_volovyk.mlkitsampling.data.dto.ReportDTO;
import com.example.serhii_volovyk.mlkitsampling.data.dto.ResponseDTO;
import com.example.serhii_volovyk.mlkitsampling.data.dto.SubscriptionDTO;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface ApiInterface {
    @POST("api/report")
    Call<ResponseDTO> sendReport(@Body ReportDTO report);

    @POST("api/subscribe")
    Call<ResponseDTO> sendSubscription(@Body SubscriptionDTO subscription);
}
