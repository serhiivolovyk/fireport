package com.example.serhii_volovyk.mlkitsampling.data.dto;

import com.google.gson.annotations.SerializedName;

public class ResponseDTO {

    @SerializedName("success")
    private String success;

    public ResponseDTO(String success) {
        this.success = success;
    }

    public String getSuccess() {
        return success;
    }
}
